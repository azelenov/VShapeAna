#ifndef REALDATATESTS_QA_H
#define REALDATATESTS_QA_H

#include "TGraphErrors.h"
#include "TH2D.h"
#include "SettingVShape.h"
#include "FittingShape.h"
#include "Derivative.h"
#include "TString.h"
#include <iostream>

class QA {
public:
  QA(TH2D *initVshape, FittingShape *fittingShape, Derivative *derivative, Long_t runNum, TString tube, TString fitType);

  virtual ~QA();
  TString imgfoldername;

private:
  TH2D *vShape;
  TGraphErrors *meanValueGraph, *sigmaGraph, *ndfGraph, *chi2_ndfGraph;
  TGraphErrors *derivativeGraph, *coordResolGraph;
  Long_t runNum;
  TString fitType, tube;

  void init();

  void drawingPics();
};


#endif //REALDATATESTS_QA_H
