#include <iostream>
#include <fstream>
#include "include/SettingVShape.h"
#include "include/FittingShape.h"
#include "include/Derivative.h"
#include "include/Resolution.h"
#include "include/QA.h"

int main()
{
	// TString dir("/Volumes/Andrew's Seagate Drive/SHiP_DATA/");
	// TString dir("/eos/experiment/ship/TestBeam/StrawTracker/2017/H2-Tests/synchronized_trees/");
	// TString fileName("Synchronized_data_run_");
	Long_t runNum = 407012;
	TString tube("det12_straw25_5mm");
	// TString ending(".root");
	// TString RootName(dir + fileName + runNum + ending);
	// TFile *AnaFile;
	// AnaFile = TFile::Open(RootName, "read");
	// TTree *mt;
	// TTree *st;
	// AnaFile->GetObject("tree", mt);
	// AnaFile->GetObject("ADC1", st);

	// TFile *f_1 = new TFile("/afs/cern.ch/user/a/azelenov/VShapeAna/vsThreshold/out_run_308106.root");
	// TFile *f_1 = new TFile("/afs/cern.ch/user/a/azelenov/straw-stand/OUT/out_run_308043.root");
	// TFile *f_1 = new TFile("../out9352_first100.root");

	// TH2D *wcut = (TH2D *)f_1->Get("SpecRTcheck/Neg0_0_0_0/N_Ch0_V0_H0_P0_S68");

	TFile *f_1 = new TFile("../SHiP way/img/FIT_FWHM_RUN_407012_TUBE_det10_straw25_5mm/binsPool.root");

	// TFile *f_1 = new TFile("../ToyMC/toyMC_bin100_r_t_smear_full.root");

	TH2D *wcut = (TH2D *)f_1->Get("vshape");
	wcut->RebinX(2);
	// TH2D *wcut = (TH2D *)f_1->Get("toyMCvshape");

	if (wcut)
		std::cout << "Get hists from NA64sw \n";
	else
		std::cout << "ERROR \n";

	// Int_t binning = 100;

	// auto *setShape = new SettingVShape(st, mt, binning);

	// std::cout << "I did /setShape/" << std::endl;

	TString fitType("gaus");
	auto *fittingShape = new FittingShape(wcut, runNum, fitType, tube);

	std::cout << "I did /fittingShape/" << std::endl;

	auto *derivative = new Derivative(fittingShape->getMeanGraph(), fittingShape->getSigmaGraph(), fittingShape->getLeftEdge(), fittingShape->getRightEdge());

	std::cout << "I did /derivative/" << std::endl;

	auto *resol = new Resolution(derivative->getCoordinateResolGraph());

	std::cout << "I did /Resolution/" << std::endl;

	auto *qa = new QA(wcut, fittingShape, derivative, runNum, tube, fitType);

	std::cout << "I did /QA/" << std::endl;

	std::ofstream out(qa->imgfoldername + "/out.txt");
	std::cout.rdbuf(out.rdbuf()); // redirect std::cout to out.txt!
	std::cout << "============================ \nThe apex is " << derivative->apex << "\nLeft Edge is " << derivative->left << "\nRight Edge is " << derivative->right << "\nSize is " << derivative->right - derivative->left << "\n============================ \n";
	std::cout << "\n\n Resolution: (" << resol->resolution << " +/- " << resol->resolutionError << ") mm \n";
	auto *resolTime = new Resolution(derivative->getTimeResolGraph());
	std::cout << "\n\n Resolution (Time): (" << resolTime->resolution << " +/- " << resolTime->resolutionError << ") ns \n";
	//  AnaFile->TFile::Close();

	return 1;
}
