#include "../include/FittingShape.h"


template<int nT>
Double_t
cbf( double x
   , double mu, double sigma
   , double alpha
   , double S
   ) {
    # if 0
    double u   = (x-mu)/width;
    double A2  = TMath::Power(p/TMath::Abs(a),p)*TMath::Exp(-a*a/2);
    double B2  = p/TMath::Abs(a) - TMath::Abs(a);

    double result(1);
    //if      (u<-a1) result *= A1*TMath::Power(B1-u,-p1);
    if (u<a)  result *= s*TMath::Exp(-u*u/2);
    else      result *= A2*TMath::Power(B2+u,-p);
    return result;
    #else
    const Double_t alpha2 = alpha*alpha
                 , absAlpha = fabs(alpha)
                 , sq2 = sqrt(2.)
                 , u = (mu - x)/sigma
                 , C = nT*exp(-alpha2/2)/((absAlpha)*(nT-1))
                 , D = sqrt(M_PI/2)*(1 + TMath::Erf(absAlpha/sq2))
                 , N = 1/(sigma*(C+D))
                 ;
    Double_t r;
    if( u <= -alpha ) {
        // sq tail
        const Double_t A = pow((nT/absAlpha), nT)*exp(-alpha2/2)
                     , B = nT/absAlpha - absAlpha
                     ;
        r = A*TMath::Power((B - (mu - x)/sigma), - nT);
    } else {
        // Gaussian
        r = TMath::Exp( -u*u/2 );
    }
    #endif
    return S*r*N;
}


double
cbfitf(double * x, double *par) {
  return(cbf<2>(x[0], par[0], par[1], par[2], par[3]));
}

FittingShape::FittingShape(TH2D *shape, Long_t runNum, TString fitType, TString tube) {
  this->shape = shape;
  this->runNum = runNum;
  this->fitType = fitType;
  this->tube = tube;
  init();
  runningBins();
  findingEdges();
}

FittingShape::~FittingShape() = default;

void FittingShape::init() {
  currentBin = new TH1D();
  meanGraph = new TGraphErrors();
  sigmaGraph = new TGraphErrors();
  ndfGraph = new TGraphErrors();
  chiSq_ndfGraph = new TGraphErrors();
}

void FittingShape::runningBins() {
  TString imgFolder("img/");
  TString mkd("mkdir -p ");
  system(mkd + imgFolder);
  TString runFolder("FIT_" + fitType + "_RUN_" + runNum + "_TUBE_" + tube);
  TString createFolder = mkd + imgFolder + runFolder;
  system(createFolder);
  TString binsFileName("/binsPool");
  TString ending(".root");
  TFile binsFile(imgFolder + runFolder + binsFileName + ending, "RECREATE");
  Int_t point = 0;

  for (int i = 1; i < shape->GetNbinsX(); ++i) {
    currentBin = shape->ProjectionY("Current bin", i, i);
    // if (currentBin->Integral() < 4000) continue;
    // if (currentBin->Integral() < 2000) continue; //run38 5mm & 10mm straw
    if (currentBin->Integral() < 200) continue; //run59 10mm straw
    fitRangeEstimation(currentBin);
    Double_t xAxisPoint = shape->GetXaxis()->GetBinCenter(i);
    // if (xAxisPoint > 0.74) continue;
    // if (xAxisPoint < 3.4 || xAxisPoint > 12.8) continue;
    Double_t chi2vsndf = 999;
    Double_t ndf = 999;
    TF1 *fitFunc;
    if (fitType == "gaus")
    {
      // while (chi2vsndf > 5 && ndf > 11)
      // {
      //   currentBin->Fit("gaus", "QR", "SAME", leftFitEdge, rightFitEdge);
      //   fitFunc = currentBin->GetFunction("gaus");
      //   chi2vsndf = fitFunc->GetChisquare() / fitFunc->GetNDF();
      //   ndf = fitFunc->GetNDF();
      //   leftFitEdge++;
      //   rightFitEdge--;
      // }
      currentBin->Fit("gaus", "QR", "SAME", leftFitEdge, rightFitEdge);
      fitFunc = currentBin->GetFunction("gaus");
      chi2vsndf = fitFunc->GetChisquare() / fitFunc->GetNDF();
      ndf = fitFunc->GetNDF();

      meanGraph->SetPoint(point, xAxisPoint, fitFunc->GetParameter(1));
      meanGraph->SetPointError(point, 0., fitFunc->GetParError(1));
      sigmaGraph->SetPoint(point, xAxisPoint, fitFunc->GetParameter(2));
      sigmaGraph->SetPointError(point, 0., fitFunc->GetParError(2));
      ndfGraph->SetPoint(point, xAxisPoint, fitFunc->GetNDF());
      chiSq_ndfGraph->SetPoint(point, xAxisPoint, fitFunc->GetChisquare() / fitFunc->GetNDF());
    }
    else if (fitType == "CB")
    {
      TF1 * funcCB = new TF1( "crystalBall", cbfitf, -140, 40, 4 );
      funcCB->SetParameters( currentBin->GetMean()  // mu
                           , currentBin->GetRMS()   // sigma
                           , .9                     // alpha
                           , currentBin->Integral()
                           );
      currentBin->Fit(funcCB, "QR", "SAME");
      meanGraph->SetPoint(point, xAxisPoint, currentBin->GetFunction("crystalBall")->GetParameter(0));
      meanGraph->SetPointError(point, 0., currentBin->GetFunction("crystalBall")->GetParError(0));
      sigmaGraph->SetPoint(point, xAxisPoint, currentBin->GetFunction("crystalBall")->GetParameter(1));
      sigmaGraph->SetPointError(point, 0., currentBin->GetFunction("crystalBall")->GetParError(1));
      ndfGraph->SetPoint(point, xAxisPoint, currentBin->GetFunction("crystalBall")->GetNDF());
      chiSq_ndfGraph->SetPoint(point, xAxisPoint, currentBin->GetFunction("crystalBall")->GetChisquare() / currentBin->GetFunction("crystalBall")->GetNDF());
    }
    else if (fitType == "FWHM")
    {
      int bin0 = currentBin->GetBinCenter(currentBin->GetMaximumBin());
      double bin0err = 1;
      int bin1 = currentBin->FindFirstBinAbove(currentBin->GetMaximum()/2);
      int bin2 = currentBin->FindLastBinAbove(currentBin->GetMaximum()/2);
      double fwhm = currentBin->GetBinCenter(bin2) - currentBin->GetBinCenter(bin1);
      double fwhmerr = TMath::Sqrt(1 * 1 * 2);
      std::cout << "Bin: " << xAxisPoint << "\t FWHM: " << fwhm << "\n";
      meanGraph->SetPoint(point, xAxisPoint, bin0);
      meanGraph->SetPointError(point, 0., bin0err);
      sigmaGraph->SetPoint(point, xAxisPoint, fwhm);
      sigmaGraph->SetPointError(point, 0., fwhmerr);
      ndfGraph->SetPoint(point, xAxisPoint, 1);
      chiSq_ndfGraph->SetPoint(point, xAxisPoint, 1);
    }

    TString binName("bin_XAxis_");
    TString num;
    num.Form("%f", xAxisPoint);
    TString fullBinName = binName + num;
    currentBin->GetXaxis()->SetTitle("T (ns)");
    currentBin->GetXaxis()->SetTitleOffset(1.4);
    currentBin->GetYaxis()->SetTitle("N entries");
    currentBin->Write(fullBinName);

    point++;
  }

}

void FittingShape::findingEdges() {
  leftEdge = 0;
  rightEdge = ndfGraph->GetN();
  Double_t *ndfVal;
  ndfVal = ndfGraph->GetY();

  for (int i = 1; i < ndfGraph->GetN() / 2; ++i) {
    if (abs(ndfVal[i] - ndfVal[i - 1]) > 20) {
      leftEdge = i;
      break;
    }
  }

  for (int j = ndfGraph->GetN() - 1; j > ndfGraph->GetN() / 2; --j) {
    if (abs(ndfVal[j] - ndfVal[j + 1]) > 20) {
      rightEdge = j;
      break;
    }
  }
}

void FittingShape::fitRangeEstimation(TH1D *binHist) {
  Double_t maxValue = binHist->GetMaximum();
  Int_t maxValueBin = binHist->GetMaximumBin();
  leftFitEdge = binHist->GetBinCenter(binHist->FindFirstBinAbove());
  rightFitEdge = binHist->GetBinCenter(binHist->FindLastBinAbove());

  for (int i = maxValueBin - 1; i > 0; --i) {
    if (binHist->GetBinContent(i) < maxValue / 10) {
      leftFitEdge = binHist->GetBinCenter(i);
      break;
    }
  }

  for (int j = maxValueBin + 1; j < binHist->FindLastBinAbove(); ++j) {
    if (binHist->GetBinContent(j) < maxValue / 10) {
      rightFitEdge = binHist->GetBinCenter(j);
      break;
    }
  }
}
