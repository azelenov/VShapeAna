#include "../include/Derivative.h"
#include <iostream>
#include <fstream>
#include "TF1.h"

Derivative::Derivative(TGraphErrors *meanGraph, TGraphErrors *sigmaGraph, Int_t leftEdge, Int_t rightEdge)
{
	this->meanGraph = meanGraph;
	this->sigmaGraph = sigmaGraph;
	this->leftEdge = leftEdge;
	this->rightEdge = rightEdge;
	derivative = new TGraphErrors();
	coordinateResolGraph = new TGraphErrors();
	derivativeCalc();
}

Derivative::~Derivative() = default;

void Derivative::derivativeCalc()
{
	Double_t *x, *y, *ySigma;
	x = meanGraph->GetX();
	y = meanGraph->GetY();
	ySigma = sigmaGraph->GetY();
	meanGraph->Fit("pol2");
	TF1 *fitFunc = meanGraph->GetFunction("pol2");
	apex = -fitFunc->GetParameter(1) / (2 * fitFunc->GetParameter(2));
	left = x[leftEdge];
	right = x[rightEdge - 1];
	Int_t point = 0;
	Double_t dX = 0;
	Double_t derivativeValue = 0;
	for (int i = leftEdge + 2; i < rightEdge - 2; i++)
	{
		derivativeValue = ((y[i - 1] - y[i - 2]) / (x[i - 1] - x[i - 2]) + (y[i] - y[i - 1]) / (x[i] - x[i - 1]) +
						   (y[i + 1] - y[i]) / (x[i + 1] - x[i]) + (y[i + 2] - y[i + 1]) / (x[i + 2] - x[i + 1])) /
						  4;
		dX = x[i] - x[i - 1];
		derivative->SetPoint(point, x[i], derivativeValue);

		Double_t dY_errorSquare1 = meanGraph->GetErrorY(i - 2) * meanGraph->GetErrorY(i - 2) +
								   meanGraph->GetErrorY(i - 1) * meanGraph->GetErrorY(i - 1);
		Double_t dY_errorSquare2 = meanGraph->GetErrorY(i - 1) * meanGraph->GetErrorY(i - 1) +
								   meanGraph->GetErrorY(i) * meanGraph->GetErrorY(i);
		Double_t dY_errorSquare3 = meanGraph->GetErrorY(i) * meanGraph->GetErrorY(i) +
								   meanGraph->GetErrorY(i + 1) * meanGraph->GetErrorY(i + 1);
		Double_t dY_errorSquare4 = meanGraph->GetErrorY(i + 1) * meanGraph->GetErrorY(i + 1) +
								   meanGraph->GetErrorY(i + 2) * meanGraph->GetErrorY(i + 2);
		Double_t derivativeError = (1. / 4.) * sqrt((1 / dX) * (1 / dX) *
													(dY_errorSquare1 + dY_errorSquare2 + dY_errorSquare3 +
													 dY_errorSquare4));
		derivative->SetPointError(point, 0., derivativeError);
		if (abs(derivativeValue) < 1e-8)
		{
			// std::cout << derivativeValue << "\n";
			continue;
		}
		Double_t binl = x[i] - (x[2] - x[1]) / 2;
		Double_t binr = x[i] + (x[2] - x[1]) / 2;
		Double_t binShit = (fitFunc->Eval(binr) - fitFunc->Eval(binl))/2;
		TF1 *rtFunctionDeriv = new TF1("rtpol2Deriv", "[0] + [1]*x", 0, 6.0);
    	rtFunctionDeriv->SetParameter(0,0.04371);
    	rtFunctionDeriv->SetParameter(1,2*5.459);
		Double_t coordResolution = (sqrt(abs(ySigma[i]*ySigma[i]))) / abs(derivativeValue);
		// Double_t coordResolution = 0;
		// std::cout << "X_i = " << x[i] << "||| sigma^2 - err^2 = " << ySigma[i]*ySigma[i] - 4  - (rtFunctionDeriv->Eval(x[i])) * (rtFunctionDeriv->Eval(x[i])) * (0.15 * 0.15) << "\n";
		// if ((ySigma[i]*ySigma[i] - 4  - (derivativeValue) * (derivativeValue) * (0.15 * 0.15 + 0.05*0.05)) < 0) 
		// {
		// 	coordResolution = -sqrt(abs((ySigma[i]*ySigma[i] - 4  - (derivativeValue) * (derivativeValue) * (0.15 * 0.15 + 0.05*0.05)))) / abs(derivativeValue);
		// } else
		// {
			// coordResolution = sqrt(abs((ySigma[i]*ySigma[i] - 4  - (rtFunctionDeriv->Eval(x[i])) * (rtFunctionDeriv->Eval(x[i])) * (0.15 * 0.15)))) / abs(rtFunctionDeriv->Eval(x[i]));
		// }

		Double_t coordResolutionCheck = ySigma[i]*ySigma[i] + 0.15*0.15/(-1 + derivativeValue*derivativeValue*
			0.15*0.15*0.15*0.15/(derivativeValue*derivativeValue*0.15*0.15*2*2 + 2*2*2*2));
		// std::cout << "resol check = " << coordResolution << "\n";
		Double_t coordResolutionError =
			(ySigma[i] / (derivativeValue * derivativeValue)) * (ySigma[i] / (derivativeValue * derivativeValue)) *
				derivativeError * derivativeError +
			(1 / derivativeValue) * (1 / derivativeValue) * sigmaGraph->GetErrorY(i) * sigmaGraph->GetErrorY(i);
		coordResolutionError = sqrt(coordResolutionError);
		// coordinateResolGraph->SetPoint(point, x[i], sqrt(coordResolution * coordResolution - 0.03*0.03));
		coordinateResolGraph->SetPoint(point, x[i], coordResolution);
		coordinateResolGraph->SetPointError(point, 0., coordResolutionError);
		point++;
	}
}